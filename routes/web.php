<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@index');                                     //Route for homepage
Route::get('/contactus', 'WebController@contactus');                        //Route for Contact us page
Route::get('/address', 'WebController@address');                            //Route for addresspage
Route::get('/checkout', 'WebController@checkout');                          //Route for checkout page
Route::get('/changepassword', 'WebController@changepassword');              //Route for changepassword page
Route::get('/ordersuccess', 'WebController@ordersuccess');                  //Route for Order Success Page
Route::get('/profile', 'WebController@profile');                            //Route for Proile page
Route::get('/shop', 'WebController@shop');                                  //Route for Shop page
Route::get('/product','WebController@product');                             //Route for Products Page
Route::get('/cart', 'WebController@cart');                                 //Route for cart page 

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
