@extends('layouts.app')

@section('content')
<!-- <div class="banner-img"></div> -->
<div class="hero">
    <div id="user"><i class="fa fa-user-circle"  aria-hidden="true"></i></div>
    <div class="from-box">
        <div class="button-box">
            <div id="btns"></div>
            <button type="button" class="toggles-btn" onclick="signin()">Sign  In</button>
            <button type="button" class="toggles-btn" onclick="signup()">Sign Up</button>
        </div>
        <div class="social-icons">
            <i class="fa fa-facebook"></i>
            <i class="fa fa-google"></i>
            <i class="fa fa-twitter"></i>
        </div>
        <form  action="" id="signin" class="input-group">
            <input type="text" class="input-form" placeholder="User Id" required>
            <input type="password" class="input-form" placeholder="Enter Password" required>
            <input type="checkbox" class="check-box"><span>Remember Password</span>
            <input type="submit" class="submit-btns" name="submit" value="Sign Up">
            <br>
            <a href="#">Lost your password?</a>
        </form>
        <form action="" id="signup"  class="input-group">
            <input type="text" class="input-form" placeholder="User Id" required>
            <input type="email" class="input-form" placeholder="Email Id" required>
            <input type="password" class="input-form" placeholder="Enter Password" required>
            <input type="checkbox" class="check-box"><span>I agree to the terms & conditions</span>
            <input type="submit" class="submit-btns" name="submit" value="Sign Up">

        </form>
    </div>
</div>   
<script type="text/javascript">
    var x = document.getElementById("signin");
    var y = document.getElementById("signup");
    var z = document.getElementById("btns");

    function signup() {
        x.style.left = "-400px";
        y.style.left = "50px";
        z.style.left = "110px";
    } 

    function signin() {
        x.style.left = "50px";
        y.style.left = "450px";
        z.style.left = "0px";
    }
</script>    
@endsection
