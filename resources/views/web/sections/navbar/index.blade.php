

<header class="head_1st">
	<h2 class="logo"><a href="#">V-Kart</a></h2>
	<div class="search-container">
		<form action="">
			<input type="text" placeholder="Search.." name="search">
			<button type="submit"><i class="fa fa-search"></i></button>
		</form>
	</div>
	<div class="sidebar-social">
		<ul>
			<li>
				<a href="#" title="Profile" ><i class="fa fa-user"></i>
					<span>Profile</span>
				</a>
			</li>
			<li>
				<a href="#" title="Cart" ><i class="fa fa-shopping-bag"></i>
					<span>Cart</span>
				</a>
			</li>
		</ul>
	</div>
</header>