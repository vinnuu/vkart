<!DOCTYPE html>
<html>
<head>
	<title>{{config('app.name','V-Kart')}}</title>  
	<link href="{{asset('web/css/custom.css')}}" rel="stylesheet">

	<!-- Fonts start-->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;469;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Fonts end -->



    <!-- Styles start-->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('web/css/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('web/css/owl.theme.default.min.css')}}" rel="stylesheet">
    <link href="{{asset('web/css/custom.css')}}" rel="stylesheet">
    <link href="{{asset('web/css/login.css')}}" rel="stylesheet">
    <!-- Styles end-->	

</head>
<body>
    @include('web.sections.navbar.index')
	@include('web.sections.navbar.header')

	<!-- Content Section Shown on Page -->
	@yield('content')


</body>
<script type="{{asset('web/js/custom.js')}}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<!-- owl carousel start -->
        <script type="{{asset('web/js/jquery.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('web/js/jquery.min.js')}}"></script>
        <script type="{{asset('web/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript" src="{{asset('web/js/owl.carousel.min.js')}}"></script>
        <script type="text/javascript">
            $('.owl-carousel_s').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 2000,
                responsive : {
					0 : {
						items: 1,

					}
				}
            })
        </script>
        <script type="text/javascript">
            $('.owl-carousel_2').owlCarousel({
                loop:true,
                margin:10,
                nav:true,
                autoplay: true,
                autoplayTimeout: 5000,
                responsive : {
					0 : {
						items: 1,
					},
					480 : {
						items: 2,
					},
					856 : {
						items: 4,
					},
					1300 : {
						items: 6,
					}
				}
            })
        </script>
<!-- owl carousel start -->
</html>